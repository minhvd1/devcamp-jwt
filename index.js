const express = require("express");
const mongoose = require("mongoose");

require('dotenv').config();

const cors = require("cors");

const app = express();

const port = process.env.SERVER_PORT || 8000;

app.use(cors());
app.use(express.json());

mongoose.connect(process.env.MONGODB_URL)
    .then(() => {
        require("./app/models/data").initialRole();
        
        console.log("Connect successfully!");
    })
    .catch((error) => {
        console.error("Connect MongoDB Failed!");
    })

app.get("/", (req, res) => {
    res.json({
        message: "Welcome to Devcamp JWT"
    })
})

app.use("/api/auth/", require("./app/routes/auth.routes"));

app.listen(port, () => {
    console.log("App listening on port:", port);
})