const express = require("express");

const route = express.Router();

const { signIn, signUp } = require("../controllers/auth.controllers");

route.post("/signup", signUp);

route.post("/signin", signIn);

module.exports = route;