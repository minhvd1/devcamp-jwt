const roleModel = require("../models/role.model");
const userModel = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const signUp = async (req, res) => {
    try {
        const { username, email, role, password } = req.body;

        const existedAccount = await userModel.findOne({ username });

        if(existedAccount) {
            return res.status(400).json({
                message: "Username existed"
            })
        }

        const userRole = await roleModel.findOne({ name: role });

        if(!userRole) {
            return res.status(400).json({
                message: "Role invalid!"
            })
        }

        await new userModel({
            username: username,
            email: email,
            role: userRole._id,
            password: bcrypt.hashSync(password, 8)
        }).save();

        return res.status(200).json({
            message: "Register successfully"
        })

    } catch (error) {
        return res.status(500).json({
            message: "Internal Server Error"
        })
    }
}

const signIn = async (req, res) => {
    try { 
        const { username, password } = req.body;

        const existedAccount = await userModel.findOne({ username });

        if(!existedAccount) {
            return res.status(404).json({
                message: "User not found"
            })
        }
        
        // So sánh giá trị mật khẩu
        var passwordIsValid = bcrypt.compareSync(
            password,
            existedAccount.password
        )

        if(!passwordIsValid) {
            return res.status(401).json({
                message: "Invalid password"
            })
        }

        const secretKey = process.env.JWT_SECRET_KEY;
        const expiresTime = process.env.JWT_EXPIRES_TIME;

        const accessToken = jwt.sign({
                id: existedAccount._id
            },
                secretKey,
            {
                expiresIn: expiresTime
            }
        )

        return res.status(200).json({
            accessToken
        })

    } catch (error) {
        console.error(error);
        return res.status(500).json({
            message: "Internal Server Error"
        })
    }
}

module.exports = {
    signIn,
    signUp
}