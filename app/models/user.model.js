const mongoose = require("mongoose");

const userModel = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        required: true
    }, 
    password: {
        type: String,
        required: true
    },
    role: {
        type: mongoose.Types.ObjectId,
        ref: "Role"
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("User", userModel);